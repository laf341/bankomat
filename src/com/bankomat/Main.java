package com.bankomat;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.lang.*;

public class Main {
    public static class Step{
        public static int Current = -2;
        public static int Sub = 0;
    }
    public static class UserDetails{
        public static int id;
        public static String username;
        public static String password;
        public static void clear(){
            id = 0;
            username = "";
            password = "";
        }

    }
    public static void main(String[] args ) throws IOException {
            generateCsvFiles();
            Listening();
    }

    public static void Listening() throws IOException{
        try {
            String Input = "";
            int sum, userid;
            Step step = new Step();
            UserDetails User = new UserDetails();
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            do {
                switch (step.Current) {
                    case -1:
                        printQuestion("(Yes) Enter login:");
                        String username = br.readLine();
                        printQuestion("Enter password:");
                        String password = br.readLine();
                        if (verifyAuth(username, password)) {
                            System.out.println("Greetings, " + User.username + " (" + User.id + ")");
                            step.Current = 0;
                            continue;
                        } else {
                            printError("Invalid username or password, please try again!");
                            continue;
                        }
                    case 0:
                        printQuestion(
                                "Choose action:\n" +
                                        "1. Transfer money by id\n" +
                                        "2. Put money\n" +
                                        "3. Take money\n" +
                                        "4. Export history\n" +
                                        "5. Logout"
                        );
                        step.Current = Integer.parseInt(br.readLine());
                        step.Current = (step.Current >= 1 && step.Current <= 5) ? step.Current : 0;

                        continue;
                    case 1:
                        printQuestion("(1) Enter userid:");
                        userid = Integer.parseInt(br.readLine());
                        if (!verifyUserid(userid)) {
                            printError("User id " + userid + " not found!");
                            continue;
                        }
                        printQuestion("Enter sum:");
                        sum = Integer.parseInt(br.readLine());
                        transferLog(User.id, userid, sum);
                        System.out.println("(sum) Transfer completed.");
                        step.Current = 0;
                        continue;
                    case 2:
                        printQuestion("(2) Enter sum:");
                        sum = Integer.parseInt(br.readLine());
                        transferLog(User.id, 0, sum);
                        System.out.println("(sum) Putting completed.");
                        step.Current = 0;
                        continue;
                    case 3:
                        printQuestion("(3) Enter sum:");
                        sum = Integer.parseInt(br.readLine());
                        transferLog(0, User.id, sum);
                        System.out.println("(sum) Taking completed.");
                        step.Current = 0;
                        continue;
                    case 4:
                        printQuestion(
                                "(4) Choose format:\n" +
                                        "1. On screen\n" +
                                        "2. To CSV"
                        );
                        step.Sub = Integer.parseInt(br.readLine());
                        List<List<String>> Transactions = getTransactionsByUser(User.id);
                        switch (step.Sub) {
                            case 1:
                                System.out.println("   Date                   From      To      Sum   ");
                                System.out.println("----------------------------------------------------");
                                for (List<String> elem : Transactions) {
                                    for (String el : elem) {
                                        System.out.print("|  " + el + "  ");
                                    }
                                    System.out.println("\n----------------------------------------------------");
                                }
                                printQuestion("(1) Export completed to screen.");
                                step.Current = 0;
                                continue;
                            case 2:
                                FileWriter writer = new FileWriter("c:\\bankomat/share/out.csv");
                                writer.write("Date;From;To;Sum\n");
                                for (List<String> elem : Transactions) {
                                    for (String el : elem) {
                                        writer.write(el + ";");
                                    }
                                    writer.write("\n");
                                }
                                writer.flush();
                                writer.close();
                                printQuestion("(2) Export completed to out.csv.");
                                step.Current = 0;
                                continue;
                            default:
                                step.Sub = 0;
                                continue;
                        }
                    case 5:
                        printQuestion("(5) Goodbye.");
                        User.clear();
                        step.Current = -2;
                        continue;
                    default:
                        printQuestion("Dou you have existing account? (y/n)");
                        Input = br.readLine();
                        if (Input.equals("y")) {
                            step.Current = -1;
                            continue;
                        }
                        break;
                }
            } while (true);
        }catch (NumberFormatException e){
            printError("Invalid input format!");
            Listening();
        }catch (Exception e){
            printError("WTF? There was some shit.");
            Listening();
        }
    }
    public static void printQuestion(String str){
        System.out.println(str);
    }

    public static void printError(String str){
        System.out.println("ERROR: "+str);
    }

    private static boolean verifyAuth(String username, String password) throws IOException{
        List<List<String>> Users = getUsersArray();
        int index = -1;
        for( List<String> UserList: Users ) {
            index = UserList.indexOf(username);
            if(index != -1 && UserList.get(2).equals(password)){
                UserDetails.id = Integer.parseInt(UserList.get(0));
                UserDetails.username = UserList.get(1);
                UserDetails.password = UserList.get(2);
                return true;
            }
        }
        return false;
    }

    private static boolean verifyUserid(int userid) throws IOException{
        List<List<String>> Users = getUsersArray();
        int index = -1;
        for( List<String> UserList: Users ) {
            index = UserList.indexOf(""+userid);
            if(index == 0) return true;
        }
        return false;
    }

    private static List<List<String>> getUsersArray()  throws IOException{
            String str = null;
            List<List<String>> Users = new ArrayList<>();
            int userIndex = 0;
            BufferedReader br = new BufferedReader(new FileReader("c:\\bankomat/users.csv"));
            while ((str = br.readLine()) != null){
                String[] list = str.split(";");
                List<String> itemList = new ArrayList<String>(Arrays.asList(list));
                Users.add(itemList);
                userIndex++;
            }
            br.close();
            return Users;
    }

    private static List<List<String>> getTransactionsByUser(int userid)  throws IOException{
        String str = null;
        List<List<String>> Transactions = new ArrayList<>();
        int index = 0;
        BufferedReader br = new BufferedReader(new FileReader("c:\\bankomat/transactions.csv"));
        while ((str = br.readLine()) != null){
            String[] list = str.split(";");
            List<String> itemList = new ArrayList<String>(Arrays.asList(list));
            index = itemList.indexOf(Integer.toString(userid));
            if(index == 1 || index == 2){
                Transactions.add(itemList);
            }
        }
        br.close();
        return Transactions;
    }

    private static void transferLog(int from, int to, int sum) throws IOException{
        String fromer = (from == 0)?"bank":""+from;
        String toer = (to == 0)?"bank":""+to;
        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        FileWriter writer = new FileWriter("c:\\bankomat/transactions.csv", true);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        bufferedWriter.append(ft.format(date)+";"+fromer+";"+toer+";"+sum);
        bufferedWriter.newLine();
        bufferedWriter.close();
    }

    private static void generateCsvFiles()
    {
        try
        {
            FileWriter writer = new FileWriter("c:\\bankomat/users.csv");
            writer.write("id;username;password\n");
            writer.write("11011;admin;123456\n");
            writer.write("11022;user;123\n");
            writer.flush();
            writer.close();

            writer = new FileWriter("c:\\bankomat/transactions.csv");
            writer.write("Date;From;To;Sum\n");
            writer.flush();
            writer.close();

        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
